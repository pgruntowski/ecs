﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour {

	[SerializeField] private GameObject sphere;
	[SerializeField] private TextMeshProUGUI displayObjCount;

	private int ObjCount = 0;
	// Use this for initialization
	void Start ()
	{
		Application.targetFrameRate = 50;
		AddTransforms(1000);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.A))
			AddTransforms(1000);
	}

	void AddTransforms(int amount)
	{

		for (int i = 0; i< amount;i++)
		{
			GameObject gameObject = Instantiate(sphere,new Vector3(Random.Range(-10f,10f),Random.Range(-10f,10f),Random.Range(-10f,10f)),Quaternion.Euler(0f,0f,0f)) as GameObject;
		}

		ObjCount += amount;
		displayObjCount.text = "ObjCount: " + ObjCount;
	}
}
