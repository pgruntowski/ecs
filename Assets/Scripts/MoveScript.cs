﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour
{
	private Vector3 direction;


	private void Start()
	{
		direction = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));
	}

	// Update is called once per frame
	void Update () {
		
		transform.position += (direction * Time.deltaTime);
		direction -= transform.position * Time.deltaTime * 0.1f;
	}
}
