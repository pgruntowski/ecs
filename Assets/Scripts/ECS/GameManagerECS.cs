﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class GameManagerECS : MonoBehaviour
{

	public Mesh mesh;
	public Material material;
	public TextMeshProUGUI amount;
	
	private EntityManager _entityManager;
	private EntityArchetype _entityArchetype;
	private MeshInstanceRenderer mir;
	private int count = 0;
	
	// Use this for initialization
	void Start () {
		_entityManager = World.Active.GetOrCreateManager<EntityManager>();
		_entityArchetype = _entityManager.CreateArchetype(ComponentType.Create<MeshInstanceRendererComponent>());
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.A))
			AddEntity(1000);
	}

	void AddEntity(int amount)
	{
		for (int i = 0; i < amount; i++)
		{
			Entity _entity = _entityManager.CreateEntity(_entityArchetype);
		
			_entityManager.AddSharedComponentData(_entity,new MeshInstanceRenderer(){
				mesh = this.mesh,
				material = this.material,
				castShadows = ShadowCastingMode.Off,
				receiveShadows = false,
				subMesh = 0
			});
			float3 temp = new float3(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));
			float tempLenght = Mathf.Sqrt(temp.x * temp.x + temp.y * temp.y + temp.z * temp.z);
			temp.x /= tempLenght;
			temp.y /= tempLenght;
			temp.z /= tempLenght;
			_entityManager.AddComponentData(_entity,new Position()
			{
				Value = (temp * 10f)
			});
			_entityManager.AddComponentData(_entity,new MoveDirection()
			{
				Value = temp
			});
		}
		count += amount;
		this.amount.text = "ObjCount: " + count;
	}
	
	
}
