﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Jobs;

public class MovmentComponentJob : JobComponentSystem  {

	[BurstCompile]
	struct MovmentJob : IJobProcessComponentData<Position,MoveDirection>
	{
		public float dt;

//		public void Execute(ref Position data)
//		{
//			float3 pos = data.Value;
//			
//			data.Value = pos;
//		}

		public void Execute(ref Position data0, ref MoveDirection data1)
		{
			data0.Value += (data1.Value * dt);
			data1.Value -= data0.Value * dt * 0.1f;
			data0.Value.x += (dt * 5f);
		}
	}
	
	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var job = new MovmentJob() { dt = Time.deltaTime };
		return job.Schedule(this, inputDeps);
	} 
	
	
}

public struct MoveDirection : IComponentData
{
	public float3 Value;
}
