﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class FpsDisplay : MonoBehaviour
{
	private TextMeshProUGUI display;

	
	
	private void Start()
	{
		display = GetComponent<TextMeshProUGUI>();
	}

	void Update ()
	{
		display.text = (1f / Time.unscaledDeltaTime).ToString("00") + " FPS (" + (Time.deltaTime*1000).ToString("F") + "ms)";
	}
}
