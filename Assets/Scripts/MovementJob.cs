﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Jobs;


[ComputeJobOptimization]
public struct MovementJob : IJobParallelForTransform
{
	public float deltaTime;
	
	public void Execute(int index, TransformAccess transform)
	{
//		Vector3 pos = transform.position;
//		pos += (transform.rotation * new Vector3(0,0,1)) * deltaTime;
//		transform.position = pos;
		
		transform.position += (transform.rotation * new Vector3(0,0,1)) * deltaTime;
	}
}
