﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
using UnityEngine.Jobs;

public class GameManagerJobSystem : MonoBehaviour
{
	private TransformAccessArray transforms;
	private MovementJob moveJob;
	private JobHandle movehandle;

	[SerializeField] private GameObject sphere;
	[SerializeField] private TextMeshProUGUI displayObjCount;
	
	
	// Use this for initialization
	void Start ()
	{
		Application.targetFrameRate = 50;
		transforms = new TransformAccessArray(0);
		AddTransforms(1000);
	}
	
	// Update is called once per frame
	void Update () {
		movehandle.Complete();
		
		if(Input.GetKeyDown(KeyCode.A))
			AddTransforms(1000);
		
		moveJob = new MovementJob()
		{
			deltaTime = Time.deltaTime
		};
		movehandle = moveJob.Schedule(transforms);
		
		JobHandle.ScheduleBatchedJobs();
	}

	private void OnDisable()
	{
		movehandle.Complete();
		transforms.Dispose();
	}

	void AddTransforms(int amount)
	{
		movehandle.Complete();
		transforms.capacity = transforms.length + amount;
		for (int i = 0; i< amount;i++)
		{
			GameObject gameObject = Instantiate(sphere,new Vector3(Random.Range(-10f,10f),0f,Random.Range(-10f,10f)),Quaternion.Euler(0f,Random.Range(0f,360f),0f)) as GameObject;
			transforms.Add(gameObject.transform);
		}

		displayObjCount.text = "ObjCount: "+ transforms.length;
	}
}
